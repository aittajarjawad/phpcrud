<?php
// On inclut la connexion à la base
require_once('connect.php');

// On écrit notre requête
$sql = 'SELECT * FROM bookmark';

// On prépare la requête
$query = $db->prepare($sql);

// On exécute la requête
$query->execute();

// On stocke le résultat dans un tableau associatif
$result = $query->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Bookmark</title>
</head>


<body>
    <main>
        <div class="container">
            <h2>Liste bookmark :</h2>

            <table>
                <thead>
                    <th>Nom</th>
                    <th>URL</th>
                </thead>
                <tbody>
                    <?php
                    foreach ($result as $bookmark) {
                    ?>
                        <tr>
                            <td><?= $bookmark['name'] ?></td>
                            <td><a class="url" href="<?= $bookmark['url'] ?>" target="_blank"><?= $bookmark['url'] ?></a></td>
                            <td>
                                <a class="modifier" href="edit.php?id=<?= $bookmark['id'] ?>">Modifier</a>
                                <a class="delete" href="delete.php?id=<?= $bookmark['id'] ?>">Supprimer</a>

                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                    <form action="index.php"><button>Bookmark Doc</button></form>
                </tbody>
            </table>
        </div>
    </main>

</body>
</div>

</html>