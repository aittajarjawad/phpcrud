<?php
require_once('connect.php');

if (isset($_GET['id']) && !empty($_GET['id'])) {
    $id = strip_tags($_GET['id']);
    $sql = "DELETE FROM bookmark WHERE id = :id";
    $query = $db->prepare($sql);
    $query->bindValue(':id', $id, PDO::PARAM_INT);
    $query->execute();

    $_SESSION['message'] = "Le bookmark a été supprimé avec succès.";
    header('Location: index.php');
}
?>
