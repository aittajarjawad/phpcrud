<?php
require_once('connect.php');

if (isset($_POST)) {
    if (
        isset($_POST['name']) && !empty($_POST['name'])
        && isset($_POST['url']) && !empty($_POST['url'])
        && isset($_POST['category']) && !empty($_POST['category'])
    ) {
        $name = strip_tags($_POST['name']);
        $url = strip_tags($_POST['url']);
        $category = strip_tags($_POST['category']);

        // Récupérer l'ID de la catégorie correspondante
        $sql = "SELECT id FROM categories WHERE name = :category";
        $query = $db->prepare($sql);
        $query->bindValue(':category', $category, PDO::PARAM_STR);
        $query->execute();
        $categoryID = $query->fetchColumn();

        $sql = "INSERT INTO bookmark(name, url, category_id) VALUES (:name, :url, :categoryID)";
        $query = $db->prepare($sql);
        $query->bindValue(':name', $name, PDO::PARAM_STR);
        $query->bindValue(':url', $url, PDO::PARAM_STR);
        $query->bindValue(':categoryID', $categoryID, PDO::PARAM_INT);
        $query->execute();

        $_SESSION['message'] = "Le bookmark a été ajouté avec succès.";
        header('Location: index.php');
    }
}
?>
