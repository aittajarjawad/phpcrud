<?php
require_once('connect.php');

if (isset($_POST)) {
    if (
        isset($_POST['id']) && !empty($_POST['id'])
        && isset($_POST['name']) && !empty($_POST['name'])
        && isset($_POST['url']) && !empty($_POST['url'])
        && isset($_POST['category']) && !empty($_POST['category'])
    ) {
        $id = strip_tags($_POST['id']);
        $name = strip_tags($_POST['name']);
        $url = strip_tags($_POST['url']);
        $category = strip_tags($_POST['category']);

        // Récupérer l'ID de la catégorie correspondante
        $sql = "SELECT id FROM categories WHERE name = :category";
        $query = $db->prepare($sql);
        $query->bindValue(':category', $category, PDO::PARAM_STR);
        $query->execute();
        $categoryID = $query->fetchColumn();

        $sql = "UPDATE bookmark SET name = :name, url = :url, category_id = :categoryID WHERE id = :id";
        $query = $db->prepare($sql);
        $query->bindValue(':name', $name, PDO::PARAM_STR);
        $query->bindValue(':url', $url, PDO::PARAM_STR);
        $query->bindValue(':categoryID', $categoryID, PDO::PARAM_INT);
        $query->bindValue(':id', $id, PDO::PARAM_INT);
        $query->execute();

        $_SESSION['message'] = "Le bookmark a été modifié avec succès.";
        header('Location: index.php');
    }
}

if (isset($_GET['id']) && !empty($_GET['id'])) {
    $id = strip_tags($_GET['id']);
    $sql = "SELECT b.*, c.name as category_name FROM bookmarks b JOIN categories c ON b.category_id = c.id WHERE b.id = :id";
    $query = $db->prepare($sql);
    $query->bindValue(':id', $id, PDO::PARAM_INT);
    $query->execute();
    $bookmark = $query->fetch(PDO::FETCH_ASSOC);
}
?>

<!-- Formulaire pré-rempli avec les informations du bookmark -->
<form action="edit.php" method="post">
    <input type="hidden" name="id" value="<?= $bookmark['id'] ?>">
    <label for="name">Nom</label><br>
    <input type="text" name="name" id="name" value="<?= $bookmark['name'] ?>"><br><br>
    <label for="url">URL</label><br>
    <input type="text" name="url" id="url" value="<?= $bookmark['url'] ?>"><br><br>
    <label for="category">Catégorie</label><br>
    <input type="text" name="category" id="category" value="<?= $bookmark['category_name'] ?>"><br><br>
    <button>Modifier</button>
</form>
